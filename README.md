
https://blog.csdn.net/weixin_43291055/article/details/122196938

# 命令行方式
antlr4 -visitor Calculator.g4

```
$ antlr4 -no-listener -visitor xxx.g4  //生成visitor接口
$ antlr4 Rows.g4                       // 生成java代码 默认为listener的方式
$ antlr4 -Dlanguage=JavaScript MyGrammar.g4 // 生成JavaScript API
```

# maven方式
mvn clean antlr4:antlr4


