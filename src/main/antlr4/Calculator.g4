// file: Calculator.g4
grammar Calculator;

line : expr33 EOF ;
expr33 : '(' expr33 ')'             # parenExpr
     | expr33 ('*'|'/') expr33      # multOrDiv
     | expr33 ('+'|'-') expr33      # addOrSubstract
     | FLOAT                        # float
     ;

WS : [ \t\n\r]+ -> skip;
FLOAT : DIGIT+ '.' DIGIT* EXPONENT?
      | '.' DIGIT+ EXPONENT?
      | DIGIT+ EXPONENT?
      ;

fragment DIGIT : '0'..'9' ;
fragment EXPONENT : ('e'|'E') ('+'|'-')? DIGIT+ ;
