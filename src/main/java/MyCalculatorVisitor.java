public class MyCalculatorVisitor extends CalculatorBaseVisitor<Object> {
  @Override
  public Object visitParenExpr(CalculatorParser.ParenExprContext ctx) {
    return visit(ctx.expr33());
  }

  @Override
  public Object visitMultOrDiv(CalculatorParser.MultOrDivContext ctx) {
    Object obj0 = ctx.expr33(0).accept(this);
    Object obj1 = ctx.expr33(1).accept(this);

    System.out.println(ctx.getChild(0).getText());

    if ("*".equals(ctx.getChild(1).getText())) {
      return (Float) obj0 * (Float) obj1;
    } else if ("/".equals(ctx.getChild(1).getText())) {
      return (Float) obj0 / (Float) obj1;
    }
    return 0f;
  }

  @Override
  public Object visitAddOrSubstract(CalculatorParser.AddOrSubstractContext ctx) {
    Object obj0 = ctx.expr33(0).accept(this);
    Object obj1 = ctx.expr33(1).accept(this);

    if ("+".equals(ctx.getChild(1).getText())) {
      return (Float) obj0 + (Float) obj1;
    } else if ("-".equals(ctx.getChild(1).getText())) {
      return (Float) obj0 - (Float) obj1;
    }
    return 0f;
  }

  @Override
  public Object visitFloat(CalculatorParser.FloatContext ctx) {
    return Float.parseFloat(ctx.getText());
  }
}

